﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;
using Diplo;
using Stego;
using TRex;
using Trice;
using Dino;

namespace MesozoicConsole
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Dinosaur louis = new Stegausaurus("Louis", 12);
            Console.WriteLine(louis.sayHello());
            Console.ReadKey();
        }
    }
}
